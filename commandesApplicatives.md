## Kafka internal commands

To allow internal commands

    unset JMX_PORT

To see topics

    kafka-topics.sh --list --zookeeper <IP Zoopeeker>:2181 

To see logs for one topic

    kafka-console-consumer.sh --zookeeper <IP Zoopeeker>:2181 --topic <nom du topic> --from-beginning --max-messages 1

to see consumers

    kafka-consumer-groups.sh  --zookeeper <nom FQDN du serveur zookeeper>:2181 -list

    kafka-consumer-groups.sh  --bootstrap-server <nom FQDN du serveur kafka>:9092 --list 

## Elasticsearch commands

For deleting index

    curl -XDELETE -u "elastic:<user elastic password>" http://<Elastic IP>:<Elastic port>/.monitor*
    curl -XDELETE -u "elastic:<user elastic password>" http://<Elastic IP>:<Elastic port>/<index name>

For checking indexes

    curl -u "elastic:<user elastic password>" http://<Elastic IP>:<Elastic port>/_cat/indices?pretty
    curl -u "elastic:<user elastic password>" http://<Elastic IP>:<Elastic port>/_cat/indices?pretty

To see Index mapping

    curl -GET -u "elastic:<user elastic password>" http://<Elastic IP>:<Elastic port>/<index name>/_mapping?pretty

To see Index specific template

    curl -GET -u "elastic:<user elastic password>" http://<Elastic IP>:<Elastic port>/_template/<index name>?pretty

To put Index specific template

     curl -X PUT -u "elastic:<user elastic password>" -H 'Content-Type: application/json' --data-binary @<nom du fichier>.json http://<Elastic IP>:<Elastic port>/_template/<nom du template>

To put Elastic licenses

    curl -XPUT -u elastic 'http://<host>:<port>/_xpack/license' -d @license.json


