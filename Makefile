# .SUFFIXES: .html .md
# 
SRCS := $(wildcard markdown/*.md)
HTML := $(SRCS:markdown/%.md=%.html)
vpath %.md markdown
vpath %.html html

default: example.html

build: $(HTML) index.md
	pandoc --standalone --quiet -c pandoc.css -o html/index.html index.md
#	cp -R images reveal.js css/pandoc.css html
	cp -R images css/pandoc.css html
%.html: %.md
	pandoc --standalone --metatag title=test -c pandoc.css -o html/$@ $<

