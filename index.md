---
title: Pres
---

## https://erynx.gitlab.io/pres/

- [Test](./test.html)
- [Commandes Applicatives](./commandesApplicatives.html)
- [Commandes Vim](./commandesVim.html)
- [Commandes Tmux et Terminator](./commandesTmuxTerminator.html)
- [Commandes Docker](./commandesDocker.html)

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```